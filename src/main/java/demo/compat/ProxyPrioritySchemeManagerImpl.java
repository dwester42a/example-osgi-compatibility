package demo.compat;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import javax.inject.Inject;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by daniel on 4/23/18.
 */
public class ProxyPrioritySchemeManagerImpl implements ProxyPrioritySchemeManager
{

    private final PrioritySchemeManager prioritySchemeManager;

    public ProxyPrioritySchemeManagerImpl(PrioritySchemeManager prioritySchemeManager)
    {
        this.prioritySchemeManager = checkNotNull(prioritySchemeManager, "prioritySchemeManager");
    }


    public boolean hasPrioritySchemes()
    {
        return true;
    }

    public List<FieldConfigScheme> getAllSchemes()
    {
        return this.prioritySchemeManager.getAllSchemes();
    }

}
