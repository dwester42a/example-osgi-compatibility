package demo.compat;


import com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager;
import org.osgi.framework.BundleContext;

/**
 * Created by daniel on 4/24/18.
 */
public class PrioritySchemeServiceFactory extends OptionalService<PrioritySchemeManager>
{
    public PrioritySchemeServiceFactory(final BundleContext bundleContext)
    {
        super(bundleContext, PrioritySchemeManager.class);
    }

    public ProxyPrioritySchemeManager get()
    {
        PrioritySchemeManager prioritySchemeManager = getService();
        return new ProxyPrioritySchemeManagerImpl( prioritySchemeManager);
    }
}
