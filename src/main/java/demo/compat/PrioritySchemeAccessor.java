package demo.compat;

/**
 * Created by daniel on 4/24/18.
 */
public interface PrioritySchemeAccessor
{
    public ProxyPrioritySchemeManager getPrioritySchemeManager();
}
