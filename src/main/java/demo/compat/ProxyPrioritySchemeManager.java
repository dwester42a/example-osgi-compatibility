package demo.compat;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.project.Project;

import java.util.List;

/**
 * Created by daniel on 4/24/18.
 */
public interface ProxyPrioritySchemeManager
{
    public boolean hasPrioritySchemes();
    public List<FieldConfigScheme> getAllSchemes();
}
