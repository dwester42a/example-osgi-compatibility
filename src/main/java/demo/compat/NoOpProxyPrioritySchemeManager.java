package demo.compat;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.jira.project.Project;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 4/24/18.
 */
public class NoOpProxyPrioritySchemeManager implements ProxyPrioritySchemeManager
{
    public List<FieldConfigScheme> getAllSchemes()
    {
        return new ArrayList<>();
    }
    public boolean hasPrioritySchemes()
    {
        return false;
    }

}
