package demo.compat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import javax.inject.Inject;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by daniel on 4/24/18.
 */
@Component
public class PrioritySchemeAccessorImpl implements PrioritySchemeAccessor
{
    private static final Logger log = LoggerFactory.getLogger(PrioritySchemeAccessorImpl.class);

    private final ApplicationContext applicationContext;
    private ProxyPrioritySchemeManager proxyPrioritySchemeManager;

    @Inject
    public PrioritySchemeAccessorImpl(ApplicationContext applicationContext)
    {
        this.applicationContext = checkNotNull(applicationContext, "applicationContext");
    }

    @Override
    public ProxyPrioritySchemeManager getPrioritySchemeManager()
    {
        if( proxyPrioritySchemeManager==null)
        {
            initProxyPrioritySchemeManager();
        }
        return proxyPrioritySchemeManager;
    }


    private void initProxyPrioritySchemeManager()
    {
        try
        {
            Class<?> prioritySchemeServiceFactoryClass = getPrioritySchemeManagerServiceFactoryClass();
            if (prioritySchemeServiceFactoryClass != null)
            {
                this.proxyPrioritySchemeManager = ((PrioritySchemeServiceFactory)applicationContext.getAutowireCapableBeanFactory().
                        createBean(prioritySchemeServiceFactoryClass, AutowireCapableBeanFactory.AUTOWIRE_CONSTRUCTOR, false)).get();
            }
        }
        catch (Exception e)
        {
            log.error("Could not create PrioritySchemeServiceFactory", e);
        }


        if( this.proxyPrioritySchemeManager==null)
        {
            log.info("Proxy failed. Going with NoOp");
            this.proxyPrioritySchemeManager = new NoOpProxyPrioritySchemeManager();
        }
    }


    private Class<?> getPrioritySchemeManagerServiceFactoryClass()
    {
        try
        {
            getClass().getClassLoader().loadClass("com.atlassian.jira.issue.fields.config.manager.PrioritySchemeManager");

            return getClass().getClassLoader().loadClass("demo.compat.PrioritySchemeServiceFactory");
        }
        catch (Exception e)
        {
            return null;
        }
    }
}
