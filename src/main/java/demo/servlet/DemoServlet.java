package demo.servlet;

import com.atlassian.jira.issue.fields.config.FieldConfigScheme;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import demo.compat.PrioritySchemeAccessor;
import demo.compat.ProxyPrioritySchemeManager;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by daniel on 4/26/18.
 */
@Scanned
public class DemoServlet extends HttpServlet
{
    private final PrioritySchemeAccessor prioritySchemeAccessor;

    @Inject
    public DemoServlet(PrioritySchemeAccessor prioritySchemeAccessor)
    {
        this.prioritySchemeAccessor = prioritySchemeAccessor;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/plain");

        PrintWriter writer = resp.getWriter();

        ProxyPrioritySchemeManager proxyPrioritySchemeManager = prioritySchemeAccessor.getPrioritySchemeManager();

        if( proxyPrioritySchemeManager.hasPrioritySchemes())
        {

            writer.write("This version does have priority schemes!");
            writer.write("\n");
            writer.write("The following schemes exist:");
            for(FieldConfigScheme fieldConfigScheme: proxyPrioritySchemeManager.getAllSchemes())
            {
                writer.write( fieldConfigScheme.getName());
                writer.write("\n");
            }
        }
        else
        {
            writer.write("This Jira version does not have priority schemes");
        }
        writer.close();

    }
}
